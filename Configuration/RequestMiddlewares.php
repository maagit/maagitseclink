<?php
declare(strict_types=1);

// define $before variable, based on loaded features
$before = [
	'typo3/cms-frontend/base-redirect-resolver',
];
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('redirects'))
{
	$before[] = 'typo3/cms-redirects/redirecthandler';
}

// register file delivery middleware to check, if there is a secured download link
return [
	'frontend' => [
		'maagit/maagitseclink/file-delivery' => [
			'target' => \Maagit\Maagitseclink\Middleware\FileDeliveryMiddleware::class,
            'after' => [
				'typo3/cms-frontend/authentication',
			],
			'before' => $before,
		],
    ],
];