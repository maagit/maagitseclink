<?php
namespace Maagit\Maagitseclink\Factory;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitseclink
	Package:			Factory
	class:				SecureLinkFactory

	description:		Create the secure link uri, based on given parameters.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


use Firebase\JWT\JWT;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\UserAspect;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class SecureLinkFactory
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     *
     * @var string
     */
	protected $resourceUri = '';
	
    /**
     *
     * @var int
     */
    protected $linkTimeout = 0;
	
    /**
     *
     * @var int
     */
	protected $expireTime = 86400;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    /**
     * Constructor.
     *
     * @param	string				$resourceUri		the resource uri to make secure
	 * @param	int					$expireTime			the time in seconds, when link expires
     * @return	void
     */
	public function __construct(string $resourceUri, $expireTime = NULL)
    {
        $this->resourceUri = $resourceUri;
   		if ($expireTime !== NULL)
		{
			$this->expireTime = $expireTime;
		}
		$this->linkTimeout = $this->calculateLinkLifetime();
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * Builds a uri which uses a PHP Script to access the resource
	 * by taking several parameters into account.
     *
     * @param	-
     * @return	string					the url
     */
    public function getUrl(): string
    {
        $resourceUri = $this->resourceUri;
        $hash = md5($resourceUri);

        $url = sprintf(
            '%s/%s%s/%s',
            'download',
            'sdl-',
            $this->getJsonWebToken(),
            pathinfo($resourceUri, PATHINFO_BASENAME)
        );

        return $url;
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * Create a json web token
     *
     * @param	-
     * @return	string					the json web token
     */
    protected function getJsonWebToken(): string
    {
        $payload = [
            'iat' => time(),
            'exp' => $this->linkTimeout,
            'user' => '',
            'groups' => '',
            'file' => $this->resourceUri,
            'page' => '',
        ];
        return JWT::encode($payload, $GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'], 'HS256');
    }

    /**
     * Calculate lifetime from now
     *
     * @param	-
     * @return	int					the expire date/time as unixtimestamp
     */
    protected function calculateLinkLifetime(): int
    {
        return $GLOBALS['EXEC_TIME'] + $this->expireTime;
    }


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
