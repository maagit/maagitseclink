<?php
namespace Maagit\Maagitseclink\Resource;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitseclink
	Package:			Resource
	class:				FileDelivery

	description:		Contains file delivery methods

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-12-27	Urs Maag		Method "getHeaderData", remove
													- Pragma => 'private'
													- Content-Length
													to avoid response error on safari
													browsers
						2024-01-17	Urs Maag		Added "request" in method "getTypolink"

------------------------------------------------------------------------------------- */


use Maagit\Maagitseclink\Utility\MimeTypeUtility;
use Firebase\JWT\JWT;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\UserAspect;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Http\Stream;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;

class FileDelivery
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var int
     */
    protected $expiryTime;

    /**
     * @var string
     */
    protected $file;
	
    /**
     * @var int
     */
    protected $fileSize;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    /**
     * Constructor.
     *
     * @param	string				$jwt			the json web token
     * @return	void
     */
	public function __construct(?string $jwt=null)
    {
        if ($jwt !== null) {
            $this->getDataFromJsonWebToken($jwt);
		}
		if ($this->expiryTime < time())
		{
			$this->exitScript('Link Expired. Access denied!', HttpUtility::HTTP_STATUS_403);
		}
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * Deliver current file object.
     *
     * @param	-
     * @return	void
     */
	public function deliver(): void
    {
		$file = GeneralUtility::getFileAbsFileName(ltrim($this->file, '/'));
        $fileName = basename($file);
        if (file_exists($file)) {
            $this->fileSize = filesize($file);
			$fileExtension = pathinfo($file, PATHINFO_EXTENSION);
            $forceDownload = true;
            $mimeType = MimeTypeUtility::getMimeType($file) ?? 'application/octet-stream';
			$header = $this->getHeaderData($mimeType, $fileName, $forceDownload);
            $this->sendHeader($header);
            $this->outputFile($file);
            die();
        }
        $this->exitScript('File does not exist!', HttpUtility::HTTP_STATUS_404);
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * Get the header data
     *
     * @param	string				$mimeType			the mime type
	 * @param	string				$fileName			the original filename
	 * @param	bool				$forceDownload		open in browser or force download?
     * @return	array									the header data
     */
	protected function getHeaderData(string $mimeType, string $fileName, bool $forceDownload): array
    {
        $header = [
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Content-Type' => $mimeType,
        ];
        if ($forceDownload === true) {
            $header['Content-Disposition'] = sprintf('attachment; filename=%s', $fileName);
        }
        return $header;
    }

    /**
     * Send the http header
     *
     * @param	array				$header				the header data
     * @return	void
     */
	protected function sendHeader(array $header): void
    {
        foreach ($header as $name => $value)
		{
            header(sprintf('%s: %s', $name, $value));
        }
    }
	
    /**
     * Write out given file
     *
     * @param	string				$file				the file
     * @return	void
     */
	protected function outputFile(string $file): void
    {
		$this->streamFile($file);
        ob_flush();
        flush();
    }
	
    /**
     * Stream given file
     *
     * @param	string				$fileName			the file
     * @return	void
     */
    protected function streamFile(string $fileName): void
    {
        $outputChunkSize = 1048576;
        $stream = new Stream($fileName);
        $stream->rewind();
        while (!$stream->eof())
		{
            echo $stream->read($outputChunkSize);
            ob_flush();
            flush();
        }
        $stream->close();
    }
	
    /**
     * Extract json web token
     *
     * @param	string				$jwt			the json web token
     * @return	void
     */
    protected function getDataFromJsonWebToken(string $jwt): void
    {
		try
		{
			$data = JWT::decode($jwt, new \Firebase\JWT\Key($GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'], 'HS256'));
	        $this->expiryTime = $data->exp;
	        $this->file = $this->getTypolink($data->file);
		}
		catch (\Exception $ex)
		{
            $this->exitScript('Hash invalid! Access denied!');
		}
    }
	
    /**
     * Exit script with given http status code
     *
     * @param	string				$message			the message
	 * @param	int					$httpStatus			the http status code
     * @return	void
     */
    protected function exitScript(string $message, $httpStatus=HttpUtility::HTTP_STATUS_403): void
    {
		header($httpStatus);
		header('Location: '.\TYPO3\CMS\Core\Utility\GeneralUtility::locationHeaderUrl('/fehler/zugriff-verweigert'));
    }
	
    /**
     * Convert typolinks in format "t3:/..." to absolute uri
     *
     * @param	string				$link				the link to convert
     * @return	string									the absolute uri
     */
	protected function getTypolink($link)
	{
		if (substr($link, 0, 4) != 't3:/')
		{
			return $link;
		}
		$cObj = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
		$request = \TYPO3\CMS\Core\Http\ServerRequestFactory::fromGlobals();
		$cObj->setRequest($request);
		$conf = array(
		    'parameter' => $link,
		    'useCashHash' => false,
		    'returnLast' => 'url',
			'forceAbsoluteUrl'  =>  false
		);
		return $cObj->typolink_URL($conf); 
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}